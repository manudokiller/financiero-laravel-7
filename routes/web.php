<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Socialite 
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('user.local_currency/{id}', 'UserController@setLocalCurrency')->name('user.local_currency');
Route::post('user/store_exchange_rates/{id}', 'UserController@store_exchange_rates')->name('user.store_exchange_rates');
Route::get('user/exchange_rates/{id}', 'UserController@exchange_rates')->name('user.exchange_rates');


Route::resource('currency', 'CurrencyController');
Route::post('currency/add', 'CurrencyController@add')->name('currency.add');

Route::resource('account', 'AccountController');
Route::post('account/{id}/transactions', 'AccountController@transactions')->name('account.transactions');

Route::resource('transaction', 'TransactionController');
Route::resource('category', 'CategoryController');