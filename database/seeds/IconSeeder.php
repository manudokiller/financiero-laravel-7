<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use App\Icon;

class IconSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $icons_file = Storage::get('files/icons.txt');
        //Elimina etiquetas no necesarios
        $icons_file = str_replace([
            '</li>'
        ], '', $icons_file);
        $icons_file = preg_replace([
            '/^\h*\v+/m', //Elimina líneas vacías
            '/^\h+|\h+$/m' //hace trim
        ], '', $icons_file);
        //Elimina los saltos de linea y reemplaza por una coma
        $icons_file =  str_replace(PHP_EOL, ',', $icons_file);
        //Separa por elemento li, para que quede cada ícono en un espacio del array
        $icons_file = explode('<li>', $icons_file);
        //El primer espacio está vacío
        for ($i=1; $i < count($icons_file)-1; $i++) { 
            //El primero y el último espacios están vacíos
            $icon = explode(',', $icons_file[$i]);
            $icon = Icon::firstOrCreate([
                'name' => $icon[1], 
                'label' => $icon[2], 
            ]);
        }
    }
}
