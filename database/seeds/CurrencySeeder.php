<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use App\Currency;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies_file = Storage::get('files/currencies.txt');
        //Elimina etiquetas no necesarios
        $currencies_file =  str_replace([
            '<table>', '</table>', 
            '<tbody>', '</tbody>', 
            '</tr>'
        ], '', $currencies_file);
        $currencies_file = preg_replace([
            '/^\h*\v+/m', //Elimina líneas vacías
            '/^\h+|\h+$/m' //hace trim
        ], '', $currencies_file);
        //Elimina la etiqueta td para que quede lo interno libre
        $currencies_file =  str_replace([
            '<td>', '</td>'
        ], '', $currencies_file);
        //Elimina los saltos de linea y reemplaza por una coma
        $currencies_file =  str_replace(PHP_EOL, ',', $currencies_file);
        //Separa por elemento tr, para que quede cada moneda en un espacio del array
        $currencies_file = explode('<tr>', $currencies_file);
        //Los dos primeros espacios no se van a necesitar ya que uno está vacío
        //y el otro tiene los títulos
        for ($i=2; $i < count($currencies_file)-1; $i++) { 
            //Al ser una tabla, originalmente, mantiene 8 espacios por línea
            //Este array es de 10 espacios
            //El primero y el último están vacíos
            $currency = explode(',', $currencies_file[$i]);
            $currency = Currency::firstOrCreate([
                'default_currency_id' => null,
                'user_id' => null,
                'name' => $currency[4], 
                'symbol' => $currency[3], 
                'description' => $currency[2],
                'country' => $currency[1]
            ]);
        }
    }
}
