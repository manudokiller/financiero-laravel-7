<?php

use Illuminate\Database\Seeder;
use App\Kind;

class KindSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kind::firstOrCreate([
            'name' => 'Transfer'
        ]);
        Kind::firstOrCreate([
            'name' => 'Expense'
        ]);
        Kind::firstOrCreate([
            'name' => 'Income'
        ]);
    }
}
