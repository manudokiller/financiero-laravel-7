<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    
    'facebook' => [
        'client_id' => '698959634335506',
        'client_secret' => '451a7aca6de92409f4647c2c6065a7a9',
        'redirect' => 'http://localhost:8000/login/facebook/callback',
    ],

    'twitter' => [
        'client_id' => 'ha4eUG7GV6HsYldTBe2f8j1Ej',
        'client_secret' => 'NGbkaVg0bvgY0ZutmVrLUMOKwRMgGZ1HbJdbvdA0hJ76ctO4Pv',
        'redirect' => 'http://localhost:8000/login/twitter/callback',
    ],

    'google' => [
        'client_id' => '1041508297747-cc8018d4hj4qaajmoddeltci9dhkpfcq.apps.googleusercontent.com',
        'client_secret' => '7XXGqHC0kBPMeloq6JOG0Ta_',
        'redirect' => 'http://127.0.0.1:8000/login/google/callback',
    ],

];
