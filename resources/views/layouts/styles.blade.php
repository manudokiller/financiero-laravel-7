<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">

<style>
  .message {
    display: block;
  }
  .message-danger {
    color: #dc3545!important;
  }
  .message-success {
    color: #14a76c!important;
  }
  .message-info {
    color: #ff652f!important;
  }
</style>

<style>
  body {
    background-color: #0b0c10;
    font-family: 'Pangolin', cursive;
  }
  #navbar {
    background-color: #1f2833;
    min-height: 10vh;
  }
  .navbar-light .navbar-brand,
  .navbar-light .navbar-nav .nav-link,
  .card-header {
    color: white !important;
  }
  .dropdown-item {
    background-color: #1f2833;
    color: white;
    border-color: #1f2833;
  }
  .dropdown-item:hover {
    background-color: #0b0c10;
    color: white;
    border-color: #1f2833;
  }
  .dropdown-menu {
    border-color: #1f2833;
    background-color: #0b0c10;
  }
  .card,
  .card-header {
    border-color: #1f2833;
  }
  .card-header,
  .card-footer {
    background-color: #1f2833;
  }
  .card-body {
    background-color: #0b0c10;
    color:  white;
  }
  .form-control,
  .form-control:read-only {
    background-color: #0b0c10;
    color:  #c5c6c7;
  }
  .form-control:focus,
  option {
    background-color: #0b0c10;
    color:  white;
  }
  input:read-only {
    pointer-events: none;
  }
  .table {
    color: white;
  }
  .table-hover>tbody>tr:hover,
  .table-hover>tbody>tr:hover>td>a>.fas,
  .table-hover>tbody>tr:hover>td>a>.far,
  .table-hover>tbody>tr:hover>td>a>.fab {
    background-color: #1f2833;
    color: white;
  }
  td>a>.fas, 
  td>a>.far, 
  td>a>.fab {
    color: white;
  }
  .btn {
    color: white !important; 
  }
  .btn:hover {
    color: #1f2833 !important; 
  }
  th {
    font-size: 1.25rem;
  }
  main{
    min-height: 90vh;
    min-width: 100vw;
  }
  .row {
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .card {
    margin-top: 1.5rem;
  }
</style>