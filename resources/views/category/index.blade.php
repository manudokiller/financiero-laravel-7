@extends('layouts.app')

@section('styles')
  <style>
    a {
      text-decoration: none;
    }
    tr {
      text-align: center;
    }
    .btn {
      margin: 1.5rem 0 1.5rem 0;
    }
    .icon-file {
      height: 2rem;
      width: 2rem;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <a class="btn btn-outline-light" href="{{ route('category.create') }}">Create</a>
    @if (session('message-success'))
      <p class="message message-success">{{ session('message-success') }}</p>
    @endif
    @if (session('message-info'))
      <p class="message message-info">{{ session('message-info') }}</p>
    @endif
    @error('error')
      <p class="message message-danger">{{ $message }}</p>
    @enderror
    <table class="table table-sm table-hover">
      <thead>
        <tr>
          <th>Father</th>
          <th>Kind</th>
          <th>Description</th>
          <th>Icon</th>
          <th>Monthly Budget</th>
          <th>Options</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($categories as $category)
          <tr>
            <td>
              @if ($category->category)
                {{ $category->category->description }}
              @endif
            </td> 
            <td>{{ $category->kind->name }}</td>
            <td>{{ $category->description }}</td>
            <td>
              @if ($category->icon)
                <i class="{{ $category->icon->name }} fa-2x"></i>
              @endif
              @if ($category->icon_file)
                <img src="{{ asset('storage/' . $category->icon_file) }}" alt="Icon" class="icon-file">
              @endif
            </td>
            <td>
              @if ($category->monthly_budget)
                {{ $category->user->localCurrency->symbol }}<span class="monthly_budget">{{ $category->monthly_budget }}</span>
              @endif
            </td> 
            <td>
              <a href="{{ route('category.edit', $category->id) }}">
                <i class="fas fa-edit fa-lg"></i>
              </a>
              -
              <a href="{{ route('category.show', $category->id) }}">
                <i class="fas fa-trash-alt fa-lg"></i>
              </a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    {{ $categories->links() }}
  </div>
@endsection
 
@section('javascript')
  <script type="text/javascript">
    window.onload =  function() {
      var select = document.getElementById("icon_name");
      var monthly_budgets = document.getElementsByClassName('monthly_budget');
      for (let i = 0; i < monthly_budgets.length; i++) {
        var value = monthly_budgets[i].innerHTML;
        monthly_budgets[i].innerHTML = ' ' + value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
      }
    }
  </script>
@endsection