@extends('layouts.app')

@section('styles')
  <style>
    .icon-file {
      height: 3rem;
      width: 3rem;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="card">
      <div class="card-header">
        <h5 class="card-title">Update Category</h5>
      </div>
      <div class="card-body">
        @error('error')
          <p class="message message-danger">{{ $message }}</p>
        @enderror
        <form method="post" action="{{ route('category.update', $category->id) }}" enctype="multipart/form-data">
          @csrf
          @method('PUT')

          <div class="row">
            @if ($category->description != 'Transfer')
              <div class="col-md-6">
                <div class="form-group">
                  <label for="category_id">Category Father:</label>
                  <select 
                  class="form-control @error('category_id') is-invalid @enderror" 
                  name="category_id" 
                  id="category">
                    <option value="">Without Father</option>
                    @foreach ($categories as $cat)
                      @if ($category->category)
                        @if ($category->category->id == $cat->id)
                          <option value="{{ $cat->id }}" selected>
                            {{ $cat->description }}
                          </option>
                        @else
                          <option value="{{ $cat->id }}">
                            {{ $cat->description }}
                          </option>
                        @endif
                      @else
                        <option value="{{ $cat->id }}">
                          {{ $cat->description }}
                        </option>
                      @endif
                    @endforeach
                  </select>
                </div>
                @error('category_id')
                  <p class="message message-danger">{{ $errors->first('category_id') }}</p>
                @enderror
                <div class="form-group">
                  <label for="kind_id">Kind:</label>
                  <select 
                  class="form-control @error('kind_id') is-invalid @enderror" 
                  name="kind_id" 
                  id="kind">
                    @foreach ($kinds as $kind)
                      @if ($category->kind->id == $kind->id)
                        <option value="{{ $kind->id }}" selected>
                          {{ $kind->name }}
                        </option>
                      @else
                        <option value="{{ $kind->id }}">
                          {{ $kind->name }}
                        </option>
                      @endif
                    @endforeach
                  </select>
                </div>
                @error('kind_id')
                  <p class="message message-danger">{{ $errors->first('kind_id') }}</p>
                @enderror
                <div class="form-group">
                  <label for="description">Description:</label>
                  <input 
                  class="form-control @error('description') is-invalid @enderror" 
                  type="text" 
                  name="description" 
                  id="description" 
                  value="{{ $category->description }}">
                </div>
                @error('description')
                  <p class="message message-danger">{{ $errors->first('description') }}</p>
                @enderror
              </div>
            @endif
            
            <div class="col-md-6">
              <div class="row">
                <div class="col-md-9">
                  <div class="form-group">
                    <label for="icon_name">We have {{ count($icons) }} icons for you. You can choose one if you wish:</label>
                    <select 
                    class="form-control @error('icon_name') is-invalid @enderror" 
                    name="icon_name" 
                    id="icon_name">
                      <option value="">Without icon</option>
                      @foreach ($icons as $icon)
                        @if ($category->icon)
                          @if ($category->icon->name == $icon->name)
                            <option value="{{ $icon->name }}" selected>
                              {{ $icon->label }}
                            </option>
                          @else
                            <option value="{{ $icon->name }}">
                              {{ $icon->label }}
                            </option>
                          @endif
                        @else
                          <option value="{{ $icon->name }}">
                            {{ $icon->label }}
                          </option>
                        @endif
                      @endforeach
                    </select>
                  </div>
                  @error('icon_name')
                    <p class="message message-danger">{{ $errors->first('icon_name') }}</p>
                  @enderror
                </div>
                <div class="col-md-3">
                  <i class="" id="select_icon"></i>
                </div>
              </div>
              <div class="row">
                <div class="col-md-10">
                  <div class="form-group">
                    <label for="icon_file">Or you can use your own icon if you wish:</label>
                    <input type="file" class="form-control-file" id="icon_file" name="icon_file" accept="image/*">
                  </div>
                  <p class="message message-info">If you select one of our icons but you upload your own, yours will be set.</p>
                  @error('icon_file')
                    <p class="message message-danger">{{ $errors->first('icon_file') }}</p>
                  @enderror
                </div>
                <div class="col-md-2">
                  @if ($category->icon_file)
                    <img src="{{ asset('storage/' . $category->icon_file) }}" alt="Icon" class="icon-file">
                  @endif
                </div>
              </div>
              @if ($category->icon_file)
                <div class="row">
                  <div class="col-md-6 offset-md-6">
                    <div class="form-check">
                      <input id="keep_icon_file" class="form-check-input" type="checkbox" name="keep_icon_file" value="true">
                      <label for="keep_icon_file" class="form-check-label">I want to keep my old icon</label>
                    </div>
                    <p class="message message-info">If you already had set your own icon and don't check this option or don't upload a new icon, your old icon will be deleted.</p>
                  </div>
                </div>
              @endif
              <div class="form-group">
                <label for="monthly_budget">Monthly Budget:</label>
                <input 
                class="form-control @error('monthly_budget') is-invalid @enderror" 
                type="number" 
                name="monthly_budget" 
                id="monthly_budget" 
                value="{{ $category->monthly_budget }}"
                step=".000001">
              </div>
              @error('monthly_budget')
                <p class="message message-danger">{{ $errors->first('monthly_budget') }}</p>
              @enderror
            </div>
          </div>
          @if ($category->icon_file)
            <div class="row">
              <div class="col-md-6 offset-md-6">
                <div class="form-check">
                  <input id="keep_icon_file" class="form-check-input" type="checkbox" name="keep_icon_file" value="true">
                  <label for="keep_icon_file" class="form-check-label">I want to keep my old icon</label>
                </div>
                <p class="message message-info">If you already had set your own icon and don't check this option or don't upload a new icon, your old icon will be deleted.</p>
              </div>
            </div>
            @endif
          
          <button type="submit" class="btn btn-outline-success">Update</button>
          <a class="btn btn-outline-primary" href="{{ route('category.index') }}">Back</a>
        </form>
      </div>
    </div>
  </div>
  
@endsection

@section('javascript')
  <script type="text/javascript">
    window.onload =  function() {
      var select = document.getElementById("icon_name");
      document.getElementById('select_icon').className = select.options[select.selectedIndex].value + ' fa-4x';
      select.onchange = function() {
        document.getElementById('select_icon').className = select.options[select.selectedIndex].value + ' fa-4x';
      }
    }
  </script>
@endsection