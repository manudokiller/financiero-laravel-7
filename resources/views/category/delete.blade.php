@extends('layouts.app')

@section('styles')
  <style>
    .icon-file {
      height: 3rem;
      width: 3rem;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="card">
      <div class="card-header">
        <h5 class="card-title">Delete Category</h5>
      </div>
      <div class="card-body">
        @error('error')
          <p class="message message-danger">{{ $message }}</p>
        @enderror
        <form method="post" action="{{ route('category.destroy', $category->id) }}" >
          @csrf
          @method('DELETE')

          <div class="row">
            @if ($category->category)
              <div class="col-md-6">
                <div class="form-group">
                  <label for="category_id">Father:</label>
                  <input id="category_id" class="form-control" type="text" name="category_id" value="{{ $category->category->description }}" readonly>
                </div>
              </div>
            @endif
            @if ($category->kind)
              <div class="col-md-6">
                <div class="form-group">
                  <label for="kind_id">Kind:</label>
                  <input id="kind_id" class="form-control" type="text" name="kind_id" value="{{ $category->kind->name }}" readonly>
                </div>
              </div>
            @endif
            <div class="col-md-6">
              <div class="form-group">
                <label for="description">Description:</label>
                <input id="description" class="form-control" type="text" name="description" value="{{ $category->description }}" readonly>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="monthly_budget">Monthly Budget:</label>
                <input id="monthly_budget" class="form-control" type="text" name="monthly_budget" value="{{ $category->user->localCurrency->symbol }} " readonly>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="icon_name">Icon:</label>
            @if ($category->icon)
              <br>
              <i class="{{ $category->icon->name }} fa-4x"></i>
            @elseif ($category->icon_file)
              <br>
              <img src="{{ asset('storage/' . $category->icon_file) }}" alt="Icon" class="icon-file">
            @else 
              <p class="message message-info">This category doesn't have an icon</p>
            @endif
          </div>
          <button type="submit" class="btn btn-outline-danger">Delete</button>
          <a class="btn btn-outline-primary" href="{{ route('category.index') }}">Back</a>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('javascript')
  <script type="text/javascript">
    window.onload =  function() {
      var value = <?php echo $category->monthly_budget; ?>;
      document.getElementById('monthly_budget').value += value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    }
  </script>
@endsection