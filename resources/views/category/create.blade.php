@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="card">
      <div class="card-header">
        <h5 class="card-title">Create an Category</h5>
      </div>
      <div class="card-body">
        <form method="post" action="{{ route('category.store') }}" enctype="multipart/form-data">
          @csrf

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="category_id">Category Father:</label>
                <select 
                class="form-control @error('category_id') is-invalid @enderror" 
                name="category_id" 
                id="category">
                  <option value="">Without Father</option>
                  @foreach ($categories as $category)
                    @if (old('category_id') == $category->id)
                      <option value="{{ $category->id }}" selected>
                        {{ $category->description }}
                      </option>
                    @else
                      <option value="{{ $category->id }}">
                        {{ $category->description }}
                      </option>
                    @endif
                  @endforeach
                </select>
              </div>
              @error('category_id')
                <p class="message message-danger">{{ $errors->first('category_id') }}</p>
              @enderror
              <div class="form-group">
                <label for="kind_id">Kind:</label>
                <select 
                class="form-control @error('kind_id') is-invalid @enderror" 
                name="kind_id" 
                id="kind">
                  @foreach ($kinds as $kind)
                    @if (old('kind_id') == $kind->id)
                      <option value="{{ $kind->id }}" selected>
                        {{ $kind->name }}
                      </option>
                    @else
                      <option value="{{ $kind->id }}">
                        {{ $kind->name }}
                      </option>
                    @endif
                  @endforeach
                </select>
              </div>
              @error('kind_id')
                <p class="message message-danger">{{ $errors->first('kind_id') }}</p>
              @enderror
              <div class="form-group">
                <label for="description">Description:</label>
                <input 
                class="form-control @error('description') is-invalid @enderror" 
                type="text" 
                name="description" 
                id="description" 
                value="{{ old('description') }}">
              </div>
              @error('description')
                <p class="message message-danger">{{ $errors->first('description') }}</p>
              @enderror
            </div>
            <div class="col-md-6">
              <div class="row">
                <div class="col-md-9">
                  <div class="form-group">
                    <label for="icon_name">We have {{ count($icons) }} icons for you. You can choose one if you wish:</label>
                    <select 
                    class="form-control @error('icon_name') is-invalid @enderror" 
                    name="icon_name" 
                    id="icon_name">
                      <option value="">Without icon</option>
                      @foreach ($icons as $icon)
                        @if (old('icon_name') == $icon->name)
                          <option value="{{ $icon->name }}" selected>
                            {{ $icon->label }}
                          </option>
                        @else
                          <option value="{{ $icon->name }}">
                            {{ $icon->label }}
                          </option>
                        @endif
                      @endforeach
                    </select>
                  </div>
                  @error('icon_name')
                    <p class="message message-danger">{{ $errors->first('icon_name') }}</p>
                  @enderror
                </div>
                <div class="col-md-3">
                  <i class="" id="select_icon"></i>
                </div>
              </div>
              <div class="form-group">
                <label for="icon_file">Or you can use your own icon if you wish:</label>
                <input type="file" class="form-control-file" id="icon_file" name="icon_file" accept="image/*">
              </div>
              <p class="message message-info">If you select one of our icons but you upload your own, yours will be set.</p>
              @error('icon_file')
                <p class="message message-danger">{{ $errors->first('icon_file') }}</p>
              @enderror
              <div class="form-group">
                <label for="monthly_budget">Monthly Budget:</label>
                <input 
                class="form-control @error('monthly_budget') is-invalid @enderror" 
                type="number" 
                name="monthly_budget" 
                id="monthly_budget" 
                value="{{ old('monthly_budget') }}"
                step=".000001">
              </div>
              @error('monthly_budget')
                <p class="message message-danger">{{ $errors->first('monthly_budget') }}</p>
              @enderror
            </div>
          </div>
          <button type="submit" class="btn btn-outline-success">Create</button>
          <a class="btn btn-outline-primary" href="{{ route('category.index') }}">Back</a>
        </form>
      </div>
    </div>
  </div>
  
@endsection

@section('javascript')
  <script type="text/javascript">
    window.onload =  function() {
      var select = document.getElementById("icon_name");
      document.getElementById('select_icon').className = select.options[select.selectedIndex].value + ' fa-4x';
      select.onchange = function() {
        document.getElementById('select_icon').className = select.options[select.selectedIndex].value + ' fa-4x';
      }
    }
  </script>
@endsection