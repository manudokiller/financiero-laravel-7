@extends('layouts.app')

@section('styles')
  <style>
    a {
      text-decoration: none;
    }
    .fa-hand-holding-heart {
      color: red;
    }
    tr {
      text-align: center;
    }
    .btn {
      margin: 1.5rem 0 1.5rem 0;
    }
    .icon-file {
      height: 2rem;
      width: 2rem;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <a class="btn btn-outline-light" href="{{ route('account.create') }}">Create</a>
    @if (session('message-success'))
      <p class="message message-success">{{ session('message-success') }}</p>
    @endif
    @if (session('message-info'))
      <p class="message message-info">{{ session('message-info') }}</p>
    @endif
    @error('error')
      <p class="message message-danger">{{ $message }}</p>
    @enderror
    @if ($errors)
      @foreach ($errors->all() as $message)
        <p class="message message-danger">{{ $message }}</p>
      @endforeach
    @endif
    <table class="table table-sm table-hover">
      <thead>
        <tr>
          <th>Name</th>
          <th>Currency</th>
          <th>Description</th>
          <th>Icon</th>
          <th>Balance</th>
          <th>Transactions</th>
          <th>Options</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($accounts as $account)
          <tr>
            <td>{{ $account->name }}</td>
            <td>{{ $account->currency->name }}</td>
            <td>{{ $account->description }}</td>
            <td>
              @if ($account->icon)
                <i class="{{ $account->icon->name }} fa-2x"></i>
              @endif
              @if ($account->icon_file)
                <img src="{{ asset('storage/' . $account->icon_file) }}" alt="Icon" class="icon-file">
              @endif
            </td>
            <td>{{ $account->currency->symbol }} <span class="balance">{{ $account->balance }}</span></td>
            <td>
              <form id="{{ $account->id }}_form" method="post" action="{{ route('account.transactions', ['id' => $account->id]) }}">
                @csrf
                <input type="hidden" name="action" value="all">
                <a href="#" onclick="document.getElementById('{{ $account->id }}_form').submit();">
                  <i class="fas fa-arrows-alt fa-lg"></i>
                </a>
              </form>
            </td>
            <td>
              <a href="{{ route('account.edit', $account->id) }}">
                <i class="fas fa-edit fa-lg"></i>
              </a>
              -
              <a href="{{ route('account.show', $account->id) }}">
                <i class="fas fa-trash-alt fa-lg"></i>
              </a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    {{ $accounts->links() }}
  </div>
@endsection

@section('javascript')
  <script type="text/javascript">
    window.onload =  function() {
      var select = document.getElementById("icon_name");
      var balances = document.getElementsByClassName('balance');
      for (let i = 0; i < balances.length; i++) {
        var value = balances[i].innerHTML;
        balances[i].innerHTML = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
      }
    }
  </script>
@endsection