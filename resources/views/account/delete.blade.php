@extends('layouts.app')

@section('styles')
  <style>
    .icon-file {
      height: 3rem;
      width: 3rem;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="card">
      <div class="card-header">
        <h5 class="card-title">Delete Account</h5>
      </div>
      <div class="card-body">
        <form method="post" action="{{ route('account.destroy', $account->id) }}" >
          @csrf
          @method('DELETE')

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="name">Name:</label>
                <input id="name" class="form-control" type="text" name="name" value="{{ $account->name }}" readonly>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="currency_id">Currency:</label>
                <input id="currency_id" class="form-control" type="text" name="currency_id" value="{{ $account->currency->name }} - {{ $account->currency->symbol }} - {{ $account->currency->country }}" readonly>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="description">Description:</label>
                <input id="description" class="form-control" type="text" name="description" value="{{ $account->description }}" readonly>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="balance">Balance:</label>
                <input id="balance" class="form-control" type="text" name="balance" value="{{ $account->currency->symbol }} " readonly>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="icon_name">Icon:</label>
            @if ($account->icon)
              <br>
              <i class="{{ $account->icon->name }} fa-4x"></i>
            @elseif ($account->icon_file)
              <br>
              <img src="{{ asset('storage/' . $account->icon_file) }}" alt="Icon" class="icon-file">
            @else 
              <p class="message message-info">This account doesn't have an icon</p>
            @endif
          </div>
          <button type="submit" class="btn btn-outline-danger">Delete</button>
          <a class="btn btn-outline-primary" href="{{ route('account.index') }}">Back</a>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('javascript')
  <script type="text/javascript">
    window.onload =  function() {
      var value = <?php echo $account->balance; ?>;
      document.getElementById('balance').value += value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    }
  </script>
@endsection