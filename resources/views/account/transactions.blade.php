@extends('layouts.app')

@section('styles')
  <style>
    a {
      text-decoration: none;
    }
   tr {
      text-align: center;
    }
    .btn {
      margin: 1.5rem 0 1.5rem 0;
    }
    .card>.card-body>.nav {
      background-color: transparent;
    }
    .tab-pane {
      padding: 0.75rem;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            {{ $account->description }} account's transactions
          </div>
          <div class="card-body">
            <table class="table table-sm table-hover">
              <thead>
                <tr>
                  <th>Amount</th>
                  <th>Kind</th>
                  <th>Detail</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($transactions as $transaction)
                  <tr>
                    <td>{{ $account->currency->symbol }} <span class="amount">{{ $transaction->amount }}</span></td>
                    <td>{{ $transaction->kind->name }}</td>
                    <td>{{ $transaction->detail }}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            {{ $transactions->links() }}
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            Search
            @error('error')
              <p class="message message-danger">{{ $message }}</p>
            @enderror
          </div>
          <div class="card-body">
            <nav>
              <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" href="#nav-all" id="nav-home-tab" data-toggle="tab" role="tab" aria-controls="nav-home" aria-selected="true">
                  All
                </a>
                <a class="nav-item nav-link" href="#nav-byDates" id="nav-profile-tab" data-toggle="tab" role="tab" aria-controls="nav-profile" aria-selected="false">
                  By Dates
                </a>
                <a class="nav-item nav-link" href="#nav-lastMonth" id="nav-contact-tab" data-toggle="tab" role="tab" aria-controls="nav-contact" aria-selected="false">
                  Last Month
                </a>
                <a class="nav-item nav-link" href="#nav-lastYear" id="nav-contact-tab" data-toggle="tab" role="tab" aria-controls="nav-contact" aria-selected="false">
                  Last Year
                </a>
                <a class="nav-item nav-link" href="#nav-aMonth" id="nav-contact-tab" data-toggle="tab" role="tab" aria-controls="nav-contact" aria-selected="false">
                  A Month
                </a>
                <a class="nav-item nav-link" href="#nav-aYear" id="nav-contact-tab" data-toggle="tab" role="tab" aria-controls="nav-contact" aria-selected="false">
                  A Year
                </a>
              </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
              <div class="tab-pane fade show active" id="nav-all" role="tabpanel" aria-labelledby="nav-home-tab">
                <form method="post" action="{{ route('account.transactions', ['id' => $account->id]) }}">
                  @csrf
                  <input type="hidden" name="action" value="all">
                  <button type="submit" class="btn btn-outline-info">Search</button>
                </form>
              </div>
              <div class="tab-pane fade" id="nav-byDates" role="tabpanel" aria-labelledby="nav-profile-tab">
                <form method="post" action="{{ route('account.transactions', ['id' => $account->id]) }}">
                  @csrf
                  <input type="hidden" name="action" value="byDates">
                  <div class="form-group">
                    <label for="from">From:</label>
                    <input id="from" class="form-control" type="date" name="from" max="{{ date('Y-m-d',strtotime('-1 days',strtotime(date('Y-m-d')))) }}"  required>
                  </div>
                  <div class="form-group">
                    <label for="to">To:</label>
                    <input id="to" class="form-control" type="date" name="to" max="{{ date('Y-m-d') }}" required>
                  </div>
                  <button type="submit" class="btn btn-outline-info">Search</button>
                </form>
              </div>
              <div class="tab-pane fade" id="nav-lastMonth" role="tabpanel" aria-labelledby="nav-contact-tab">
                <form method="post" action="{{ route('account.transactions', ['id' => $account->id]) }}">
                  @csrf
                  <input type="hidden" name="action" value="lastMonth">
                  <button type="submit" class="btn btn-outline-info">Search</button>
                </form>
              </div>
              <div class="tab-pane fade" id="nav-lastYear" role="tabpanel" aria-labelledby="nav-contact-tab">
                <form method="post" action="{{ route('account.transactions', ['id' => $account->id]) }}">
                  @csrf
                  <input type="hidden" name="action" value="lastYear">
                  <button type="submit" class="btn btn-outline-info">Search</button>
                </form>
              </div>
              <div class="tab-pane fade" id="nav-aMonth" role="tabpanel" aria-labelledby="nav-contact-tab">
                <form method="post" action="{{ route('account.transactions', ['id' => $account->id]) }}">
                  @csrf
                  <input type="hidden" name="action" value="aMonth">
                  <div class="form-group">
                    <label for="year">Year:</label>
                    <input id="year" class="form-control" type="number" name="year" max="{{ intval(date('Y')) }}" min="{{ intval(date('Y')) - 100 }}" required>
                  </div>
                  <div class="form-group">
                    <label for="month">Month:</label>
                    <input id="month" class="form-control" type="number" name="month" max="12" min="1" required>
                  </div>
                  <button type="submit" class="btn btn-outline-info">Search</button>
                </form>
              </div>
              <div class="tab-pane fade" id="nav-aYear" role="tabpanel" aria-labelledby="nav-contact-tab">
                <form method="post" action="{{ route('account.transactions', ['id' => $account->id]) }}">
                  @csrf
                  <input type="hidden" name="action" value="aYear">
                  <div class="form-group">
                    <label for="year">Year:</label>
                    <input id="year" class="form-control" type="number" name="year" max="{{ intval(date('Y')) }}" min="{{ intval(date('Y')) - 100 }}" required>
                  </div>
                  <button type="submit" class="btn btn-outline-info">Search</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('javascript')
  <script type="text/javascript">
    window.onload =  function() {
      var amounts = document.getElementsByClassName('amount');
      for (let i = 0; i < amounts.length; i++) {
        var value = amounts[i].innerHTML;
        amounts[i].innerHTML = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
      }
    }
  </script>
@endsection