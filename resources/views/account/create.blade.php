@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="card">
      <div class="card-header">
        <h5 class="card-title">Create an Account</h5>
      </div>
      <div class="card-body">
        <form method="post" action="{{ route('account.store') }}" enctype="multipart/form-data">
          @csrf

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="name">Name:</label>
                <input 
                  id="name" 
                  class="form-control @error('name') is-invalid @enderror" 
                  type="text" 
                  name="name"
                  value="{{ old('name') }}">
              </div>
              @error('name')
                <p class="message message-danger">{{ $errors->first('name') }}</p>
              @enderror
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="currency_id">Currency:</label>
                <select class="form-control @error('currency_id') is-invalid @enderror" name="currency_id" id="currency">
                  @foreach ($currencies as $currency)
                    @if (old('currency_id') == $currency->id)
                      <option value="{{ $currency->id }}" selected>
                        {{ $currency->country }} - {{ $currency->name }} - {{ $currency->description }}
                      </option>
                    @else
                      <option value="{{ $currency->id }}">
                        {{ $currency->country }} - {{ $currency->name }} - {{ $currency->description }}
                      </option>
                    @endif
                  @endforeach
                </select>
              </div>
              @error('currency_id')
                <p class="message message-danger">{{ $errors->first('currency_id') }}</p>
              @enderror
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="description">Description:</label>
                <input 
                  id="description" 
                  class="form-control @error('description') is-invalid @enderror" 
                  type="text" 
                  name="description"
                  value="{{ old('description') }}">
              </div>
              @error('description')
                <p class="message message-danger">{{ $errors->first('description') }}</p>
              @enderror
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="balance">Balance:</label>
                <input 
                  id="balance" 
                  class="form-control @error('balance') is-invalid @enderror" 
                  type="number" 
                  step=".000001"
                  name="balance"
                  value="{{ old('balance') }}">
              </div>
              @error('balance')
                <p class="message message-danger">{{ $errors->first('balance') }}</p>
              @enderror
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="row">
                <div class="col-md-10">
                  <div class="form-group">
                    <label for="icon_name">We have {{ count($icons) }} icons for you. You can choose one if you wish:</label>
                    <select 
                    class="form-control @error('icon_name') is-invalid @enderror" 
                    name="icon_name" 
                    id="icon_name">
                      <option value="">Without icon</option>
                      @foreach ($icons as $icon)
                        @if (old('icon_name') == $icon->name)
                          <option value="{{ $icon->name }}" selected>
                            {{ $icon->label }}
                          </option>
                        @else
                          <option value="{{ $icon->name }}">
                            {{ $icon->label }}
                          </option>
                        @endif
                      @endforeach
                    </select>
                  </div>
                  @error('icon_name')
                    <p class="message message-danger">{{ $errors->first('icon_name') }}</p>
                  @enderror
                </div>
                <div class="col-md-2">
                  <i class="" id="select_icon"></i>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="icon_file">Or you can use your own icon if you wish:</label>
                <input type="file" class="form-control-file" id="icon_file" name="icon_file" accept="image/*">
              </div>
              <p class="message message-info">If you select one of our icons but you upload your own, yours will be set.</p>
              @error('icon_file')
                <p class="message message-danger">{{ $errors->first('icon_file') }}</p>
              @enderror
            </div>
          </div>
          <button type="submit" class="btn btn-outline-success">Create</button>
          <a class="btn btn-outline-primary" href="{{ route('account.index') }}">Back</a>
        </form>
      </div>
    </div>
  </div>
  
@endsection

@section('javascript')
  <script type="text/javascript">
    window.onload =  function() {
      var select = document.getElementById("icon_name");
      document.getElementById('select_icon').className = select.options[select.selectedIndex].value + ' fa-4x';
      select.onchange = function() {
        document.getElementById('select_icon').className = select.options[select.selectedIndex].value + ' fa-4x';
      }
    }
  </script>
@endsection