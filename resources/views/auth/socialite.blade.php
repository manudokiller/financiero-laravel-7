<div id="socialite-section">
  <p class="socialite-or">OR</p>
  <a href="{{ url('login/facebook') }}" class="socialite-link">
    <button type="button" class="btn btn-block btn-outline-light text-dark shadow">
      <img class="socialite-logo" src="{{ asset('storage/socialite_logos/facebook.png') }}" alt="tag"> &sdot; Continue with Facebook
    </button>
  </a>
  <a href="{{ url('login/twitter') }}" class="socialite-link">
    <button type="button" class="btn btn-block btn-outline-light text-dark shadow">
      <img class="socialite-logo" src="{{ asset('storage/socialite_logos/twitter.png') }}" alt="tag"> &sdot; Continue with Twitter
    </button>
  </a>
  <a href="{{ url('login/google') }}" class="socialite-link">
    <button type="button" class="btn btn-block btn-outline-light text-dark shadow">
      <img class="socialite-logo" src="{{ asset('storage/socialite_logos/google.png') }}" alt="tag"> &sdot; Continue with Google
    </button>
  </a>
</div>