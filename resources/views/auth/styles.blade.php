<style>
  #socialite-section{
    display: flex;
    align-items: center;
    text-align: center;
    flex-direction: column;
    border-top-style: solid;
    border-top-color: rgb(151, 160, 175);
    border-top-width: 0.1rem;
    margin-top: 1rem;
    padding-top: 0.25rem;
  }
  .socialite-link, .socialite-or {
    padding-top: 0.25rem;
    padding-bottom: 0.25rem;
    width: 50%;
    text-decoration: none !important;
  }
  .socialite-or {
    color: rgb(151, 160, 175);
    margin: 0;
  }
  .socialite-logo {
    width: 1.75rem;
    height: 1.75rem;
  }
</style>