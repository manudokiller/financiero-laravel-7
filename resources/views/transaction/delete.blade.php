@extends('layouts.app')

@section('styles')
  <style>
    .icon-file {
      height: 3rem;
      width: 3rem;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="card">
      <div class="card-header">
        <h5 class="card-title">Delete Transaction</h5>
      </div>
      <div class="card-body">
        <form method="post" action="{{ route('transaction.destroy', $transaction->id) }}" >
          @csrf
          @method('DELETE')

          <div class="row">

            <div class="col-md-4">
              @if ($transaction->pair)
                <div class="form-group">
                  <label for="account_from">Account From:</label>
                  <input
                    readonly  
                    id="account_from" 
                    class="form-control" 
                    type="text"
                    name="account_from" 
                    value="{{ $transaction_from->account->description }}"
                  >
                </div>
                <div class="form-group">
                  <label for="account_from">Account To:</label>
                  <input
                    readonly  
                    id="account_from" 
                    class="form-control" 
                    type="text"
                    name="account_from" 
                    value="{{ $transaction_to->account->description }}"
                  >
                </div>
              @else
                <div class="form-group">
                  <label for="account_from">Account From:</label>
                  <input
                    readonly  
                    id="account_from" 
                    class="form-control" 
                    type="text"
                    name="account_from" 
                    value="{{ $transaction->account->description }}"
                  >
                </div>
              @endif
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label for="kind_id">Kind:</label>
                <input
                  readonly  
                  id="kind_id" 
                  class="form-control" 
                  type="text"
                  name="kind_id" 
                  value="{{ $transaction->kind->name }}"
                >
              </div>
              <div class="form-group">
                <label for="category_id">Category:</label>
                <input
                  readonly  
                  id="category_id" 
                  class="form-control" 
                  type="text"
                  name="category_id" 
                  value="{{ $transaction->category->description }}"
                >
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label for="date">Date:</label>
                <input
                  readonly  
                  id="date" 
                  class="form-control" 
                  type="text"
                  name="date" 
                  value="{{ $transaction->date }}"
                >
              </div>
            </div>

          </div>

          <div class="row">

            <div class="col-md-6">
              <div class="form-group">
                <label for="amount">Amount:</label>
                <input
                  readonly  
                  id="amount" 
                  class="form-control" 
                  type="text" 
                  name="amount"
                  value="{{ $transaction->amount }}">
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="detail">Detail:</label>
                <input
                  readonly  
                  id="detail" 
                  class="form-control" 
                  type="text" 
                  name="detail"
                  value="{{ $transaction->detail }}">
              </div>
            </div>

          </div>
          <button type="submit" class="btn btn-outline-danger">Delete</button>
          <a class="btn btn-outline-primary" href="{{ route('transaction.index') }}">Back</a>
        </form>
      </div>
    </div>
  </div>
@endsection