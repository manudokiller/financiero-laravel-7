@extends('layouts.app')

@section('styles')
  <style>
    a {
      text-decoration: none;
    }
    tr {
      text-align: center;
    }
    .btn {
      margin: 1.5rem 0 1.5rem 0;
    }
    .icon-file {
      height: 2rem;
      width: 2rem;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <a class="btn btn-outline-light" href="{{ route('transaction.create') }}">Create</a>
    @if (session('message-success'))
      <p class="message message-success">{{ session('message-success') }}</p>
    @endif
    @if (session('message-info'))
      <p class="message message-info">{{ session('message-info') }}</p>
    @endif
    @error('error')
      <p class="message message-danger">{{ $message }}</p>
    @enderror
    <table class="table table-sm table-hover">
      <thead>
        <tr>
          <th>Account</th>
          <th>Category</th>
          <th>Amount</th>
          <th>Kind</th>
          <th>Detail</th>
          <th>Options</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($transactions as $transaction)
          <tr>
            <td>{{ $transaction->account->name }}</td>
            <td>{{ $transaction->category->description }}</td>
            <td>{{ $transaction->amount }}</td>
            <td>{{ $transaction->kind->name }}</td>
            <td>{{ $transaction->detail }}</td>
            <td>
              <a href="{{ route('transaction.edit', $transaction->id) }}">
                <i class="fas fa-edit fa-lg"></i>
              </a>
              -
              <a href="{{ route('transaction.show', $transaction->id) }}">
                <i class="fas fa-trash-alt fa-lg"></i>
              </a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    {{ $transactions->links() }}
  </div>
@endsection