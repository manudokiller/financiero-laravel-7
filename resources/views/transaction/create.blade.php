@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="card">
      <div class="card-header">
        <h5 class="card-title">Create an Transaction</h5>
      </div>
      <div class="card-body">
        <form method="post" action="{{ route('transaction.store') }}">
          @csrf

          <div class="row">

            <div class="col-md-4">
              <div class="form-group">
                <label for="account_from">Account From:</label>
                <select class="form-control @error('account_from') is-invalid @enderror" name="account_from" id="account">
                  @foreach ($accounts as $account)
                    @if (old('account_from') == $account->id)
                      <option value="{{ $account->id }}" selected>
                        {{ $account->name }} - {{ $account->currency->name }}
                      </option>
                    @else
                      <option value="{{ $account->id }}">
                        {{ $account->name }} - {{ $account->currency->name }}
                      </option>
                    @endif
                  @endforeach
                </select>
              </div>
              @error('account_from')
                <p class="message message-danger">{{ $errors->first('account_from') }}</p>
              @enderror
              <div class="form-group" id="account_to" hidden>
                <label for="account_to">Account To:</label>
                <select class="form-control @error('account_to') is-invalid @enderror" name="account_to" id="account">
                  @foreach ($accounts as $account)
                    @if (old('account_to') == $account->id)
                      <option value="{{ $account->id }}" selected>
                        {{ $account->name }} - {{ $account->currency->name }}
                      </option>
                    @else
                      <option value="{{ $account->id }}">
                        {{ $account->name }} - {{ $account->currency->name }}
                      </option>
                    @endif
                  @endforeach
                </select>
              </div>
              @error('account_to')
                <p class="message message-danger">{{ $errors->first('account_to') }}</p>
              @enderror
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label for="kind_id">Kind:</label>
                <select class="form-control @error('kind_id') is-invalid @enderror" name="kind_id" id="kind" onchange="kindChanged()">
                  @foreach ($kinds as $kind)
                    @if (old('kind_id') == $kind->id)
                      <option value="{{ $kind->id }}" selected>
                        {{ $kind->name }}
                      </option>
                    @else
                      <option value="{{ $kind->id }}">
                        {{ $kind->name }}
                      </option>
                    @endif
                  @endforeach
                </select>
              </div>
              @error('kind_id')
                <p class="message message-danger">{{ $errors->first('kind_id') }}</p>
              @enderror
              <div class="form-group">
                <label for="category_id">Category:</label>
                <select class="form-control @error('category_id') is-invalid @enderror" name="category_id" id="category">
                  @foreach ($categories as $category)
                    @if (old('category_id') == $category->id)
                      <option value="{{ $category->id }}" selected>
                        {{ $category->description }}
                      </option>
                    @else
                      <option value="{{ $category->id }}">
                        {{ $category->description }}
                      </option>
                    @endif
                  @endforeach
                </select>
              </div>
              @error('category_id')
                <p class="message message-danger">{{ $errors->first('category_id') }}</p>
              @enderror
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label for="date">Date:</label>
                <input 
                  id="date" 
                  class="form-control" 
                  type="date" 
                  name="date" 
                  max="{{ date('Y-m-d') }}" 
                  value="{{ old('date') }}"
                >
              </div>
              @error('date')
                <p class="message message-danger">{{ $errors->first('date') }}</p>
              @enderror
            </div>

          </div>

          <div class="row">

            <div class="col-md-6">
              <div class="form-group">
                <label for="amount">Amount:</label>
                <input 
                  id="amount" 
                  class="form-control @error('amount') is-invalid @enderror" 
                  type="number" 
                  name="amount"
                  step=".000001"
                  min=".000001"
                  value="{{ old('amount') }}">
              </div>
              @error('amount')
                <p class="message message-danger">{{ $errors->first('amount') }}</p>
              @enderror
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="detail">Detail:</label>
                <input 
                  id="detail" 
                  class="form-control @error('detail') is-invalid @enderror" 
                  type="text" 
                  name="detail"
                  value="{{ old('detail') }}">
              </div>
              @error('detail')
                <p class="message message-danger">{{ $errors->first('detail') }}</p>
              @enderror
            </div>

          </div>

          <button type="submit" class="btn btn-outline-success">Create</button>
          <a class="btn btn-outline-primary" href="{{ route('transaction.index') }}">Back</a>
        </form>
      </div>
    </div>
  </div>
  
@endsection

@section('javascript')
  <script type="text/javascript">
    function kindChanged() {
      var kind = document.getElementById('kind');
      if (kind.options[kind.selectedIndex].text == 'Transfer') {
        document.getElementById('account_to').hidden = false;
      } else {
        document.getElementById('account_to').hidden = true;
      }
    }
    window.onload =  function() {
      kindChanged();
    }
  </script>
@endsection