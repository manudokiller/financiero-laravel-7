@extends('layouts.app')

@section('styles')
  <style>
    .icon-file {
      height: 3rem;
      width: 3rem;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="card">
      <div class="card-header">
        <h5 class="card-title">Update Transaction</h5>
      </div>
      <div class="card-body">
        <form method="post" action="{{ route('transaction.update', $transaction->id) }}">
          @csrf
          @method('PUT')

          <div class="form-group">
            <label for="date">Date:</label>
            <input 
              id="date" 
              class="form-control" 
              type="date" 
              name="date" 
              max="{{ date('Y-m-d') }}" 
              value="{{ $transaction->date }}"
            >
          </div>
          @error('date')
            <p class="message message-danger">{{ $errors->first('date') }}</p>
          @enderror
          <div class="form-group">
            <label for="amount">Amount:</label>
            <input 
              id="amount" 
              class="form-control @error('amount') is-invalid @enderror" 
              type="number" 
              name="amount"
              step=".000001"
              min=".000001"
              value="{{ $transaction->amount }}">
          </div>
          @error('amount')
            <p class="message message-danger">{{ $errors->first('amount') }}</p>
          @enderror
          <div class="form-group">
            <label for="detail">Detail:</label>
            <input 
              id="detail" 
              class="form-control @error('detail') is-invalid @enderror" 
              type="text" 
              name="detail"
              value="{{ $transaction->detail }}">
          </div>
          @error('detail')
            <p class="message message-danger">{{ $errors->first('detail') }}</p>
          @enderror
          
          <button type="submit" class="btn btn-outline-success">Update</button>
          <a class="btn btn-outline-primary" href="{{ route('transaction.index') }}">Back</a>
        </form>
      </div>
    </div>
  </div>
  
@endsection

@section('javascript')
  <script type="text/javascript">
    window.onload =  function() {
      var select = document.getElementById("icon_name");
      document.getElementById('select_icon').className = select.options[select.selectedIndex].value + ' fa-4x';
      select.onchange = function() {
        document.getElementById('select_icon').className = select.options[select.selectedIndex].value + ' fa-4x';
      }
    }
  </script>
@endsection