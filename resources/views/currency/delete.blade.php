@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="card">
      <div class="card-header">
        Delete Currency
        @error('error')
          <p class="message message-danger">{{ $errors->first('error') }}</p>
        @enderror
      </div>
      <div class="card-body">
        <form method="post" action="{{ route('currency.destroy', $currency->id) }}">
          @csrf
          @method('DELETE')

          <div class="form-group">
            <label for="name">Name:</label>
            <input 
              id="name" 
              class="form-control"
              type="text" 
              name="name"
              value="{{ $currency->name }}"
              readonly>
          </div>
          
          <div class="form-group">
            <label for="symbol">Symbol:</label>
            <input 
              id="symbol" 
              class="form-control"
              type="text" 
              name="symbol"
              value="{{ $currency->symbol }}"
              readonly>
          </div>
          
          <div class="form-group">
            <label for="description">Description:</label>
            <input 
              id="description" 
              class="form-control"
              type="text" 
              name="description"
              value="{{ $currency->description }}"
              readonly>
          </div>
          
          <div class="form-group">
            <label for="country">Country:</label>
            <input 
              id="country" 
              class="form-control"
              type="text" 
              name="country"
              value="{{ $currency->country }}"
              readonly>
          </div>
          
          <button type="submit" class="btn btn-outline-danger">Delete</button>
          <a class="btn btn-outline-primary" href="{{ route('currency.index') }}">Back</a>
        </form>
      </div>
    </div>
  </div>
@endsection