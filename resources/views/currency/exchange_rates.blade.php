@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="card">
      <div class="card-header">
        <h5 class="card-title">Set Exchange Rates</h5>
      </div>
      <div class="card-body">
        <form class="form" method="post" action="{{ route('user.store_exchange_rates', $currency->id) }}">
          @csrf
          <div class="form-group">
            <label 
              for="exchange_rate">
              {{ $currency->name }} - {{ $currency->description }} - {{ $currency->country }}
            </label>
            <input 
              id="exchange_rate" 
              class="form-control @error('name') is-invalid @enderror" 
              type="number" 
              step=".000001" 
              name="exchange_rate">
            @error('exchange_rate')
              <p class="message message-danger">{{ $errors->first('exchange_rate') }}</p>
            @enderror
          </div>
          <button type="submit" class="btn btn-outline-success">Save</button>
          <a class="btn btn-outline-primary" href="{{ route('currency.index') }}">Back</a>
        </form>
      </div>
    </div>
  </div>
@endsection