@extends('layouts.app')

@section('styles')
  <style>
    .card-text {
      padding-bottom: 1.25rem;
      margin: 0;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title">Add Currency</h5>
          </div>
          <div class="card-body">
            @if (session('message-info'))
              <p class="message message-info">{{ session('message-info') }}</p>
            @endif
            @if (count($currencies) > 0)
              <p class="card-text">Here you can add one of our default currencies</p>
              <form method="post" action="{{ route('currency.add') }}">
                @csrf
                <div class="form-group">
                  <select class="form-control @error('currency') is-invalid @enderror" name="currency" id="currency">
                    @foreach ($currencies as $currency)
                      <option value="{{ $currency->id }}">
                        {{ $currency->country }} - {{ $currency->name }} - {{ $currency->description }}
                      </option>
                    @endforeach
                  </select>
                </div>
                @error('currency')
                  <p class="message message-danger">{{ $errors->first('currency') }}</p>
                @enderror
      
                <button type="submit" class="btn btn-outline-success">Add</button>
                <a class="btn btn-outline-primary" href="{{ route('currency.index') }}">Back</a>
              </form>
            @else
              <p class="card-text">You have added all of our default currencies, try creating your own</p>
            @endif
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title">Create Currency</h5>
          </div>
          <div class="card-body">
            <p class="card-text">If you want to create a new currency, you can do it here.</p>
            <form method="post" action="{{ route('currency.store') }}">
              @csrf
    
              <div class="form-group">
                <label for="name">Name:</label>
                <input 
                  id="name" 
                  class="form-control @error('name') is-invalid @enderror" 
                  type="text" 
                  name="name"
                  value="{{ old('name') }}">
              </div>
              @error('name')
                <p class="message message-danger">{{ $errors->first('name') }}</p>
              @enderror
    
              <div class="form-group">
                <label for="symbol">Symbol:</label>
                <input 
                  id="symbol" 
                  class="form-control @error('symbol') is-invalid @enderror" 
                  type="text" 
                  name="symbol"
                  value="{{ old('symbol') }}">
              </div>
              @error('symbol')
                <p class="message message-danger">{{ $errors->first('symbol') }}</p>
              @enderror
    
              <div class="form-group">
                <label for="description">Description:</label>
                <input 
                  id="description" 
                  class="form-control @error('description') is-invalid @enderror" 
                  type="text" 
                  name="description"
                  value="{{ old('description') }}">
              </div>
              @error('description')
                <p class="message message-danger">{{ $errors->first('description') }}</p>
              @enderror
    
              <div class="form-group">
                <label for="country">Country:</label>
                <input 
                  id="country" 
                  class="form-control @error('country') is-invalid @enderror" 
                  type="text" 
                  name="country"
                  value="{{ old('country') }}">
              </div>
              @error('country')
                <p class="message message-danger">{{ $errors->first('country') }}</p>
              @enderror
    
              <button type="submit" class="btn btn-outline-success">Create</button>
              <a class="btn btn-outline-primary" href="{{ route('currency.index') }}">Back</a>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection