@extends('layouts.app')

@section('styles')
  <style>
    a {
      text-decoration: none;
    }
    .fa-hand-holding-heart {
      color: red;
    }
    tr {
      text-align: center;
    }
    .btn {
      margin: 1.5rem 0 1.5rem 0;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <a class="btn btn-outline-light" href="{{ route('currency.create') }}">Add / Create</a>
    @if (session('message-success'))
      <p class="message message-success">{{ session('message-success') }}</p>
    @endif
    @if (session('message-info'))
      <p class="message message-info">{{ session('message-info') }}</p>
    @endif
    @error('error')
      <p class="message message-danger">{{ $message }}</p>
    @enderror
    <table class="table table-sm table-hover">
      <thead>
        <tr>
          <th>Short Name</th>
          <th>Symbol</th>
          <th>Description</th>
          <th>Country</th>
          <th>Local Currency</th>
          <th>Exchange Rate</th>
          <th>Options</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($currencies as $currency)
          <tr>
            <td>{{ $currency->name }}</td>
            <td>{{ $currency->symbol }}</td>
            <td>{{ $currency->description }}</td>
            <td>{{ $currency->country }}</td>
            <td>
              @if (Auth::user()->local_currency == $currency->id)
                <i class="fas fa-hand-holding-heart fa-lg"></i>
              @else
                <a href="{{ route('user.local_currency', $currency->id) }}">
                  <i class="fas fa-hand-holding fa-lg"></i>
                </a>
              @endif
            </td>
            <td>
              @if ($currency->exchange_rate)
                {{ $currency->exchange_rate }}
              @else
                <a href="{{ route('user.exchange_rates', $currency->id) }}">
                  <i class="fas fa-hand-holding fa-lg"></i>
                </a>
              @endif
            </td>
            <td>
              <a href="{{ route('currency.edit', $currency->id) }}">
                <i class="fas fa-edit fa-lg"></i>
              </a>
              -
              <a href="{{ route('currency.show', $currency->id) }}">
                <i class="fas fa-trash-alt fa-lg"></i>
              </a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    {{ $currencies->links() }}
  </div>
@endsection