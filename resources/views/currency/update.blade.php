@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="card">
      <div class="card-header">
        Update Currency
      </div>
      <div class="card-body">
        <form method="post" action="{{ route('currency.update', $currency->id) }}">
          @csrf
          @method('PUT')

          <div class="form-group">
            <label for="name">Name:</label>
            <input 
              id="name" 
              class="form-control @error('name') is-invalid @enderror" 
              type="text" 
              name="name"
              value="{{ $currency->name }}">
          </div>
          @error('name')
            <p class="message message-danger">{{ $errors->first('name') }}</p>
          @enderror

          <div class="form-group">
            <label for="symbol">Symbol:</label>
            <input 
              id="symbol" 
              class="form-control @error('symbol') is-invalid @enderror" 
              type="text" 
              name="symbol"
              value="{{ $currency->symbol }}">
          </div>
          @error('symbol')
            <p class="message message-danger">{{ $errors->first('symbol') }}</p>
          @enderror

          <div class="form-group">
            <label for="description">Description:</label>
            <input 
              id="description" 
              class="form-control @error('description') is-invalid @enderror" 
              type="text" 
              name="description"
              value="{{ $currency->description }}">
          </div>
          @error('description')
            <p class="message message-danger">{{ $errors->first('description') }}</p>
          @enderror

          <div class="form-group">
            <label for="country">Country:</label>
            <input 
              id="country" 
              class="form-control @error('country') is-invalid @enderror" 
              type="text" 
              name="country"
              value="{{ $currency->country }}">
          </div>
          @error('country')
            <p class="message message-danger">{{ $errors->first('country') }}</p>
          @enderror

          <button type="submit" class="btn btn-outline-success">Update</button>
          <a class="btn btn-outline-primary" href="{{ route('currency.index') }}">Back</a>
        </form>
      </div>
    </div>
  </div>
@endsection