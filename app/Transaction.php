<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ['account_id', 'kind_id', 'category_id', 'pair', 'amount', 'detail', 'date'];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function kind()
    {
        return $this->belongsTo(Kind::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
