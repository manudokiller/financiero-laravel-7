<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'user_id', 'category_id', 'kind_id', 'icon_id', 'icon_file', 'description', 'monthly_budget'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function kind()
    {
        return $this->belongsTo(Kind::class);
    }

    public function icon()
    {
        return $this->belongsTo(Icon::class);
    }

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
