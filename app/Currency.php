<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = [
        'default_currency_id', 
        'user_id', 
        'name', 
        'symbol', 
        'description', 
        'country',
        'exchange_rate'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function accounts()
    {
        return $this->hasMany(Account::class);
    }

}
