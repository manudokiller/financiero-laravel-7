<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'user_id', 'currency_id', 'icon_id', 'name', 'description', 'balance', 'icon_file'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function icon()
    {
        return $this->belongsTo(Icon::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function byDatesTransactions($account_id, $from, $to)
    {
        return Transaction::where('account_id', $account_id)
            ->where('date', '>=', $from)
            ->where('date', '<=', $to);
    }

    public function lastMonthTransactions($account_id)
    {
        $lastMonth = date('Y-m-d',strtotime('-30 days',strtotime(date('Y-m-d'))));
        return Transaction::where('account_id', $account_id)
            ->where('date', '>=', $lastMonth);
    }

    public function lastYearTransactions($account_id)
    {
        $lastYear = date('Y-m-d',strtotime('-365 days',strtotime(date('Y-m-d'))));
        return Transaction::where('account_id', $account_id)
            ->where('date', '>=', $lastYear);
    }

    public function aMonthTransactions($account_id, $year, $month)
    {
        $days = 31;
        if (in_array($month, [4, 6, 9, 11])) {
            $days = 30;
        }
        if($month == 2) {
            $days = 28;
        }
        $from = $year . '-' . $month . '-1';
        $to = $year . '-' . $month . '-' . $days;
        return Transaction::where('account_id', $account_id)
            ->where('date', '>=', $from)
            ->where('date', '<=', $to);
    }

    public function aYearTransactions($account_id, $year)
    {
        $from = $year . '-1-1';
        $to = $year . '-12-31';
        return Transaction::where('account_id', $account_id)
            ->where('date', '>=', $from)
            ->where('date', '<=', $to);
    }
}
