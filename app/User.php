<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'local_currency',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function socialProviders()
    {
        return $this->hasMany(SocialProvider::class);
    }

    public function localCurrency()
    {
        return $this->belongsTo(Currency::class, 'local_currency');
    }

    public function currencies()
    {
        return $this->hasMany(Currency::class);
    }
    
    public function currenciesIds()
    {
        $ids = array();
        foreach ($this->currencies as $currency) {
            if ($currency->default_currency_id) {
                array_push($ids, $currency->default_currency_id);
            } else {
                array_push($ids, $currency->id);
            }
        }
        return $ids;
    }

    public function resetExchangeRates()
    {
        foreach ($this->currencies as $currency) {
            $currency->update([
                'exchange_rate' => null
            ]);
        }
    }

    public function accounts()
    {
        return $this->hasMany(Account::class);
    }

    public function transactions()
    {
        return $this->hasManyThrough(Transaction::class, Account::class);
    }

    public function categories()
    {
        return $this->HasMany(Category::class);
    }
}
