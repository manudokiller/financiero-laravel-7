<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use Auth;
use App\Kind;
use App\Category;
use App\Icon;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('category.index', [
            'categories' => Auth::user()->categories()->paginate(10)
        ]);
    }

    public function create()
    {
        return view('category.create', [
            'categories' => Auth::user()->categories,
            'kinds' => Kind::all(),
            'icons' => Icon::all()->sortBy('label')
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'nullable|exists:categories,id',
            'kind_id' => 'required|exists:kinds,id',
            'icon_file' => 'nullable|image',
            'icon_name' => 'nullable|exists:icons,name',
            'description' => 'required|max:255|unique:categories,description,null,null,user_id,' . Auth::user()->id,
            'monthly_budget' => 'nullable|numeric|min:0.000001'
        ]);
        $category = Category::create([
            'user_id' => Auth::user()->id,
            'category_id' => $request['category_id'],
            'kind_id' => $request['kind_id'],
            'description' => $request['description'],
            'monthly_budget' => $request['monthly_budget']
        ]);
        if ($request['icon_file']) {
            $category->update([
                'icon_file' => Storage::putFileAs(
                    'icons/' . Auth::user()->id, 
                    $request['icon_file'],
                    'category_' . $category->id . '.' . $request['icon_file']->getClientOriginalExtension()
                )
            ]);
        } elseif($request['icon_name']) {
            $category->update([
                'icon_id' => Icon::where('name', $request['icon_name'])->first()->id
            ]);
        }
        return redirect(route('category.index'))->with([
            'message-success' => 'Category created correctly!'
        ]);
    }

    public function show(Category $category)
    {
        if ($category->description == 'Transfer') {
            return redirect()->back()->withErrors([
                'error' => 'You can\'t delete this category'
            ]);
        }
        return view('category.delete', [
            'category' => $category
        ]);
    }

    public function edit(Category $category)
    {
        return view('category.update', [
            'category' => $category,
            'categories' => Category::where('user_id', Auth::user()->id)->where('id', '!=', $category->id)->get(),
            'kinds' => Kind::all(),
            'icons' => Icon::all()->sortBy('label')
        ]);
    }

    public function update(Request $request, Category $category)
    {
        $request->validate([
            'icon_file' => 'nullable|image',
            'icon_name' => 'nullable|exists:icons,name',
            'monthly_budget' => 'nullable|numeric|min:0.000001'
        ]);
        if ($category->description == 'Transfer') {
            $category->update([
                'monthly_budget' => $request['monthly_budget']
            ]);
        } else {
            $request->validate([
                'category_id' => 'nullable|exists:categories,id,id,!' . $category->id,
                'kind_id' => 'required|exists:kinds,id',
                'description' => 'required|max:255|unique:categories,description,' . $category->id . ',id,user_id,' . Auth::user()->id
            ]);
            $category->update([
                'category_id' => $request['category_id'],
                'kind_id' => $request['kind_id'],
                'description' => $request['description'],
                'monthly_budget' => $request['monthly_budget']
            ]);
        }
        if (!$request['keep_icon_file']) {
            if ($category->icon_file) {
                $category->update([
                    'icon_file' => null
                ]);
            }
            if ($request['icon_file']) {
                $category->update([
                    'icon_file' => Storage::putFileAs(
                            'icons/' . Auth::user()->id, 
                            $request['icon_file'],
                            'category_' . $category->id . '.' . $request['icon_file']->getClientOriginalExtension()
                        ),
                    'icon_id' => null
                ]);
            } elseif($request['icon_name']) {
                $category->update([
                    'icon_id' => Icon::where('name', $request['icon_name'])->first()->id,
                    'icon_file' => null
                ]);
            } else {
                $category->update([
                    'icon_id' => null,
                    'icon_file' => null
                ]);
            }
        } 
        return redirect(route('category.index'))->with([
            'message-success' => 'Account updated correctly!'
        ]);
    }

    public function destroy(Category $category)
    {
        if ($category->categories()->exists()) {
            return redirect()->back()->withErrors([
                'error' => 'This category has categories related to it, you can\'t delete it'
            ]);
        }
        if ($category->transactions()->exists()) {
            return redirect()->back()->withErrors([
                'error' => 'This category has transactions related to it, you can\'t delete it'
            ]);
        }
        $account->delete();
        return redirect(route('account.index'))->with([
            'message-success' => 'Account deleted correctly!'
        ]);
    }
}
