<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\SocialProvider;
use App\User;
use App\Category;
use App\Kind;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        try {
            if($provider =='twitter')
            {
                $socialUser = Socialite::driver($provider)->user();
            } else {
                $socialUser = Socialite::driver($provider)->stateless()->user();
            }
        } catch (Exception $e) { 
            return redirect('/login');
        }
        $socialProvider = SocialProvider::where('provider_id', $socialUser->getId())->first();
        if (!$socialProvider) {
        	$user = User::where('email', $socialUser->getEmail())->first();
        	if (!$user) {
        		$user = User::firstOrCreate([
                    'email'     => $socialUser->getEmail(),
                    'name'      => $socialUser->getName(),
                    'password'  => Hash::make(Str::random(100)),
                    'avatar'    => $socialUser->getAvatar()
                ]);
                Category::create([
                    'user_id' => $user->id,
                    'kind_id' => Kind::where('name', 'Transfer')->first()->id,
                    'description' => 'Transfer'
                ]);
        	}
            $user->socialProviders()->create(
                ['provider_id' => $socialUser->getId(), 'provider' => $provider]
            );
        }else{
            $user = $socialProvider->user;
            if (!$user->avatar) {
                $user->avatar = $socialUser->getAvatar();
                $user->save();
            }
        }
        Auth()->login($user);
        return redirect('/home');
    }
}
