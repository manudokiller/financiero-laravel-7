<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;
use Auth;

class CurrencyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('currencyNotRelated', 
            ['only' => 
                ['edit', 'update', 'show', 'destroy']
        ]);
        $this->middleware('currencyByUser', 
            ['only' => 
                ['edit', 'update']
        ]);
    }

    public function index()
    {
        return view('currency.index', [
            'currencies' => Auth::user()
                ->currencies()
                ->orderBy('country', 'ASC')
                ->paginate(10)
        ]);
    }

    public function create()
    {
        return view('currency.create', [
            'currencies' => Currency::where('user_id', null)
                            ->whereNotIn('id', 
                                Auth::user()->currenciesIds()
                            )->orderBy('country')
                            ->get()
        ]); 
    }

    public function add(Request $request)
    {
        $request->validate([
            'currency' => 'required|
                exists:currencies,id,user_id,"NULL"|
                unique:currencies,default_currency_id,null,null,user_id,' . Auth::user()->id
        ]);
        $default_currency = Currency::find($request['currency']);
        Currency::create([
            'default_currency_id' => $default_currency->id,
            'user_id' => Auth::user()->id,
            'name' => $default_currency->name,
            'symbol' => $default_currency->symbol,
            'description' => $default_currency->description,
            'country' => $default_currency->country
        ]);
        return redirect(route('currency.index'))->with([
            'message-success' => 'Currency added to your list'
        ]);
    }

    public function store(Request $request)
    {
        $currency = Currency::create($request->validate([
            'name' => 'required|unique:currencies,name,null,null,user_id,' . Auth::user()->id,
            'symbol' => 'required',
            'description' => 'required|unique:currencies,description,null,null,user_id,' . Auth::user()->id,
            'country' => 'required'
        ]));
        $currency->update([
            'user_id' => Auth::user()->id
        ]);
        return redirect(route('currency.index'))->with([
            'message-success' =>'Currency created and added to your list!'
        ]);
    }

    public function show(Currency $currency)
    {
        return view('currency.delete', [
            'currency' => $currency
        ]);
    }

    public function edit(Currency $currency)
    {
        return view('currency.update', [
            'currency' => $currency
        ]);
    }

    public function update(Request $request, Currency $currency)
    {
        $currency->update($request->validate([
            'name' => 'required|unique:currencies,name,' . $currency->name . ',name,user_id,' . Auth::user()->id,
            'symbol' => 'required',
            'description' => 'required|unique:currencies,description,' . $currency->description . ',description,user_id,' . Auth::user()->id,
            'country' => 'required'
        ]));
        return redirect(route('currency.index'))->with([
            'message-success' => 'Currency correctly updated'
        ]);
    }

    public function destroy(Currency $currency)
    {
        if ($currency->accounts()->exists()) {
            return redirect()->back()->withErrors([
                'error' => 'This currency has accounts related to it, you can\'t delete it'
            ]);
        }
        if (Auth::user()->local_currency == $currency->id) {
            Auth::user()->resetExchangeRates();
            Auth::user()->update([
                'local_currency' => null
            ]);
        }
        $currency->delete();
        return redirect(route('currency.index'))->with([
            'message-success' => 'Currency deleted correctly!'
        ]);
    }
}
