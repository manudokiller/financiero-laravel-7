<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Validator;
use Auth;
use App\Account;
use App\Currency;
use App\Icon;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('currencyByUser', 
            ['only' => 
                ['store', 'update']
        ]);
    }

    public function index()
    {
        return view('account.index', [
            'accounts' => Account::where('user_id', Auth::user()->id)->paginate(10)
        ]);
    }

    public function create()
    {
        return view('account.create', [
            'currencies' => Auth::user()
                ->currencies()
                ->orderBy('country', 'ASC')
                ->get(),
            'icons' => Icon::all()
                ->sortBy('label')
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required',
            'balance' => 'required|numeric|min:0',
            'currency_id' => 'required|exists:currencies,id',
            'name' => 'required|unique:accounts,name,null,null,user_id,' . Auth::user()->id . ',currency_id,' . $request['currency_id'],
            'icon_file' => 'nullable|image',
            'icon_name' => 'nullable|exists:icons,name'
        ]);
        $account = Account::create([
            'user_id' => Auth::user()->id, 
            'currency_id' => $request['currency_id'], 
            'name' => $request['name'], 
            'description' => $request['description'], 
            'balance' => $request['balance'], 
        ]);
        if ($request['icon_file']) {
            $account->update([
                'icon_file' => Storage::putFileAs(
                    'icons/' . Auth::user()->id, 
                    $request['icon_file'],
                    'account_' . $account->id . '.' . $request['icon_file']->getClientOriginalExtension()
                )
            ]);
        } elseif($request['icon_name']) {
            $account->update([
                'icon_id' => Icon::where('name', $request['icon_name'])->first()->id
            ]);
        }
        return redirect(route('account.index'))->with([
            'message-success' => 'Account created correctly!'
        ]);
    }

    public function show(Account $account)
    {
        return view('account.delete', [
            'account' => $account
        ]);
    }

    public function edit(Account $account)
    {
        return view('account.update', [
            'account' => $account,
            'currencies' => Auth::user()
                ->currencies()
                ->orderBy('country', 'ASC')
                ->get(),
            'icons' => Icon::all()
                ->sortBy('label')
        ]);
    }

    public function update(Request $request, Account $account)
    {
        //table[,column[,ignore value[,ignore column[,where column,where value]...]]]
        $request->validate([
            'description' => 'required',
            'balance' => 'required|numeric|min:0',
            'currency_id' => 'required|exists:currencies,id',
            'name' => 'required|unique:accounts,name,' . $account->name . ',name,user_id,' . Auth::user()->id . ',currency_id,' . $request['currency_id'],
            'icon_file' => 'nullable|image',
            'icon_name' => 'nullable|exists:icons,name'
        ]);
        $account->update([
            'currency_id' => $request['currency_id'], 
            'name' => $request['name'], 
            'description' => $request['description'], 
            'balance' => $request['balance'], 
        ]);
        if (!$request['keep_icon_file']) {
            if ($account->icon_file) {
                $account->update([
                    'icon_file' => null
                ]);
            }
            if ($request['icon_file']) {
                $account->update([
                    'icon_file' => Storage::putFileAs(
                            'icons/' . Auth::user()->id, 
                            $request['icon_file'],
                            'account_' . $account->id . '.' . $request['icon_file']->getClientOriginalExtension()
                        ),
                    'icon_id' => null
                ]);
            } elseif($request['icon_name']) {
                $account->update([
                    'icon_id' => Icon::where('name', $request['icon_name'])->first()->id,
                    'icon_file' => null
                ]);
            } else {
                $account->update([
                    'icon_id' => null,
                    'icon_file' => null
                ]);
            }
        } 
        return redirect(route('account.index'))->with([
            'message-success' => 'Account updated correctly!'
        ]);
    }

    public function destroy(Account $account)
    {
        if ($account->transactions()->exists()) {
            return redirect()->back()->withErrors([
                'error' => 'This account has transactions related to it, you can\'t delete it'
            ]);
        }
        $account->delete();
        return redirect(route('account.index'))->with([
            'message-success' => 'Account deleted correctly!'
        ]);
    }

    /* VERIFICAR QUE SEA DEL USUARIO LOGUEADO */
    public function transactions(Request $request, $account_id)
    {
        $account = Account::find($account_id);
        if ($account) {
            switch ($request['action']) {
                case 'all':
                    return view('account.transactions', [
                        'account' => $account,
                        'transactions' => $account->transactions()->paginate(7)
                    ]);
                    break;
                case 'byDates':
                    $validator = Validator::make($request->all(), [
                        'from' => 'required|date|before:to',
                        'to' => 'required|date|after:from,before:' . date('Y-m-d')
                    ]);
                    if ($validator->fails()) {
                        return redirect()->route('account.index')
                                    ->withErrors($validator->errors());
                    }
                    return view('account.transactions', [
                        'account' => $account,
                        'transactions' => $account->byDatesTransactions($account->id, $request['from'], $request['to'])->paginate(7)
                    ]);
                    break;
                
                case 'lastMonth':
                    return view('account.transactions', [
                        'account' => $account,
                        'transactions' => $account->lastMonthTransactions($account->id)->paginate(7)
                    ]);
                    break;
                
                case 'lastYear':
                    return view('account.transactions', [
                        'account' => $account,
                        'transactions' => $account->lastYearTransactions($account->id)->paginate(7)
                    ]);
                    break;
                
                case 'aMonth':
                    $validator = Validator::make($request->all(), [
                        'year' => 'required|numeric|min:' . (intval(date('Y')) - 100) . ',max:' . date('Y'),
                        'month' => 'required|numeric|between:1,12'
                    ]);
                    if ($validator->fails()) {
                        return redirect()->route('account.index')
                                    ->withErrors($validator->errors());
                    }
                    return view('account.transactions', [
                        'account' => $account,
                        'transactions' => $account->aMonthTransactions($account->id, $request['year'], $request['month'])->paginate(7)
                        ]);
                    break;
    
                case 'aYear':
                    $validator = Validator::make($request->all(), [
                        'year' => 'required|numeric|min:' . (intval(date('Y')) - 100) . ',max:' . date('Y')
                    ]);
                    if ($validator->fails()) {
                        return redirect()->route('account.index')
                                    ->withErrors($validator->errors());
                    }
                    return view('account.transactions', [
                        'account' => $account,
                        'transactions' => $account->aYearTransactions($account->id, $request['year'])->paginate(7)
                    ]);
                    break;
            }
        }
        return redirect(route('account.index'))->withErrors([
            'error' => 'Oops! something went wrong'
        ]);
    }
}