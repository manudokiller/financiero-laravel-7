<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Transaction;
use App\Account;
use App\Kind;

class TransactionController extends Controller
{
    public function index()
    {
        return view('transaction.index', [
            'transactions' => Auth::user()->transactions()->paginate(10)
        ]);
    }

    public function create()
    {
        if (!Auth::user()->localCurrency) {
            return redirect(route('currency.index'))->withErrors([
                'error' => 'Please define your local currency first!'
            ]);
        }
        return view('transaction.create', [
            'accounts' => Auth::user()->accounts,
            'kinds' => Kind::all(),
            'categories' => Auth::user()->categories
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'account_from' => 'required|exists:accounts,id,user_id,' . Auth::user()->id,
            'kind_id' => 'required|exists:kinds,id',
            'category_id' => 'required|exists:categories,id,user_id,' . Auth::user()->id,
            'amount' => 'required|numeric|min:.000001',
            'detail' => 'required',
            'date' => 'required|max:' . date('Y-m-d')
        ]);
        $account_from = Account::find($request['account_from']);
        /*
        $request->validate([
            'amount' => 'numeric|max:' . $account_from->balance
        ]);
        */
        if (!$account_from->currency->exchange_rate) {
            return redirect()->back()->withErrors([
                'account_from' => 'Please define the exchange rate for this account first!'
            ]);
        }
        $kind_selected = Kind::find($request['kind_id']);
        $kind_income = Kind::where('name', 'Income')->first();
        $kind_expense = Kind::where('name', 'Expense')->first();
        if ($kind_selected->name == 'Transfer') {
            $request->validate([
                'account_to' => 'required|exists:accounts,id,user_id,' . Auth::user()->id
            ]);
            $account_to = Account::find($request['account_to']);
            if (!$account_to->currency->exchange_rate) {
                return redirect()->back()->withErrors([
                    'account_to' => 'Please define the exchange rate for this account first!'
                ]);
            }
            $ammount_exchanged;
            switch (Auth::user()->local_currency) {
                case $account_from->currency->id:
                    $ammount_exchanged = $request['amount'] * $account_to->currency->exchange_rate;
                    break;
                case  $account_to->currency->id:
                    $ammount_exchanged = $request['amount'] / $account_to->currency->exchange_rate;
                    break;
                default:
                    $ammount_exchanged = ($request['amount'] / $account_from->currency->exchange_rate) * $account_to->currency->exchange_rate;
            }
            $account_from->update([
                'balance' => $account_from->balance - $request['amount']
            ]);
            $account_to->update([
                'balance' => $account_to->balance + $request['amount']
            ]);
            $transaction_from = Transaction::create([
                'account_id' => $account_from->id,
                'kind_id' => $kind_expense->id,
                'category_id' => $request['category_id'],
                'amount' => $request['amount'],
                'detail' => $request['detail'],
                'date' => $request['date']
            ]);
            $transaction_to = Transaction::create([
                'account_id' => $account_to->id,
                'kind_id' => $kind_income->id,
                'category_id' => $request['category_id'],
                'pair' => $transaction_from->id,
                'amount' => $ammount_exchanged,
                'detail' => $request['detail'],
                'date' => $request['date']
            ]);
            $transaction_from->update([
                'pair' => $transaction_to->id
            ]);
        } else {
            $transaction_from = Transaction::create([
                'account_id' => $account_from->id,
                'kind_id' => $request['kind_id'],
                'category_id' => $request['category_id'],
                'amount' => $request['amount'],
                'detail' => $request['detail'],
                'date' => $request['date']
            ]);
            switch ($kind_selected->name) {
                case 'Expense':
                    $account_from->update([
                        'balance' => $account_from->balance - $request['amount']
                    ]);
                    $transaction_from->update([
                        'kind_id' => $kind_expense->id
                    ]);
                    break;
                case 'Income':
                    $account_from->update([
                        'balance' => $account_from->balance + $request['amount']
                    ]);
                    $transaction_from->update([
                        'kind_id' => $kind_income->id
                    ]);
                    break;
            }
        }
        return redirect(route('transaction.index'))->with([
            'message-success' => 'Transaction created correctly!'
        ]);
    }


    public function show(Transaction $transaction)
    {
        if ($transaction->pair) {
            $transaction_from = $transaction;
            $transaction_pair = Transaction::find($transaction->pair);
            $transaction_to = $transaction_pair;
            if ($transaction->id > $transaction_pair->id) {
                $transaction_from = $transaction_pair;
                $transaction_to = $transaction;
            }
            return view('transaction.delete', [
                'transaction' => $transaction,
                'transaction_from' => $transaction_from,
                'transaction_to' => $transaction_to
            ]);
        }
        return view('transaction.delete', [
            'transaction' => $transaction
        ]);
        
    }

    public function edit(Transaction $transaction)
    {
        return view('transaction.update', [
            'transaction' => $transaction
        ]);
    }

    public function update(Request $request, Transaction $transaction)
    {
        $request->validate([
            'amount' => 'required|numeric|min:.000001|max:' . ($transaction->account->balance + $transaction->amount),
            'detail' => 'required',
            'date' => 'required|max:' . date('Y-m-d')
        ]);
        switch ($transaction->category->description) {
            case 'Transfer':
                $transaction_from = $transaction;
                $transaction_pair = Transaction::find($transaction->pair);
                $transaction_to = $transaction_pair;
                if ($transaction->id > $transaction_pair->id) {
                    $transaction_from = $transaction_pair;
                    $transaction_to = $transaction;
                }
                $ammount_exchanged =  $request['amount'] - $transaction->amount;
                switch (Auth::user()->local_currency) {
                    case $transaction_from->account->currency->id:
                        $ammount_exchanged = $request['amount'] * $transaction_to->account->currency->exchange_rate;
                        break;
                    case  $transaction_to->account->currency->id:
                        $ammount_exchanged = $request['amount'] / $transaction_to->account->currency->exchange_rate;
                        break;
                    default:
                        $ammount_exchanged = ($request['amount'] / $transaction_from->account->currency->exchange_rate) * $transaction_to->account->currency->exchange_rate;
                }
                $transaction_from->account->update([
                    'balance' => $transaction_from->account->balance + $transaction->amount - $request['amount']
                ]);
                $transaction_to->account->update([
                    'balance' => $transaction_to->account->balance - $transaction->amount + $request['amount']
                ]);
                if($transaction->id == $transaction_from->id) {
                    $transaction_from->update([
                        'amount' => $request['amount'],
                        'detail' => $request['detail'],
                        'date' => $request['date']
                    ]);
                    $transaction_to->update([
                        'amount' => $ammount_exchanged,
                        'detail' => $request['detail'],
                        'date' => $request['date']
                    ]);
                } else {
                    $transaction_from->update([
                        'amount' => $ammount_exchanged,
                        'detail' => $request['detail'],
                        'date' => $request['date']
                    ]);
                    $transaction_to->update([
                        'amount' => $request['amount'],
                        'detail' => $request['detail'],
                        'date' => $request['date']
                    ]);
                }
                break;
            
            case 'Expense':
                $transaction->account->update([
                    'balance' => $transaction->account->balance + $transaction->amount - $request['amount']
                ]);
                $transaction->update([
                    'amount' => $request['amount'],
                    'detail' => $request['detail'],
                    'date' => $request['date']
                ]);
                break;
            case 'Income':
                $transaction->account->update([
                    'balance' => $transaction->account->balance - $transaction->amount + $request['amount']
                ]);
                $transaction->update([
                    'amount' => $request['amount'],
                    'detail' => $request['detail'],
                    'date' => $request['date']
                ]);
                break;
        }
        return redirect(route('transaction.index'))->with([
            'message-success' => 'Transaction created correctly!'
        ]);
    }

    public function destroy(Transaction $transaction)
    {
        if ($transaction->pair) {
            $transaction_pair = Transaction::find($transaction->pair);
            $transaction_pair->delete();
        }
        $transaction->delete();
        return redirect(route('transaction.index'))->with([
            'message-success' => 'Transaction deleted correctly!'
        ]);
    }
}
