<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Currency;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('currencyByUser', 
            ['only' => 
                ['exchange_rates', 'store_exchange_rates']
        ]);
    }

    public function setLocalCurrency($id)
    {
        if (!Currency::find($id)) {
            return redirect()->back()->with([
                'message-danger' => 'Currency incorrect'
            ]);
        }
        Auth::user()->update([
            'local_currency' => $id
        ]);
        $this->fresh_exchange_rates($id);
        return redirect()->back()->with([
            'message-success' => 'Local Currency set'
        ]);
    }

    private function fresh_exchange_rates($id){
        foreach (Auth::user()->currencies as $currency) {
            if ($currency->id == $id) {
                $currency->update([
                    'exchange_rate' => 1
                ]);
            } else {
                $currency->update([
                    'exchange_rate' => null
                ]);
            }
        }
    }

    public function exchange_rates($id)
    {
        $currency = Currency::find($id);
        if ($currency) {
            return view('currency.exchange_rates', [
                'currency' => $currency
            ]);
        }
        return redirect()->back()->withErrors([
            'error' => 'Something went wrong'
        ]);
    }

    public function store_exchange_rates(Request $request, $id)
    {
        $currency = Currency::find($id);
        $currency->update([
            'exchange_rate' => $request->validate([
                    'exchange_rate' => 'numeric|min:0|not_in:0'
                ])['exchange_rate']
        ]);
        return redirect(route('currency.index'));
    }
}
