<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class KindByUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->kind) {
            if(!$request->kind->default_kind_id && $request->kind->user_id) {
                return $next($request);
            }
            return redirect(route('kind.index'))->withErrors([
                'error' => 'You are not allowed to update this kind!'
            ]);
        } elseif ($request->id) {
            if (Auth::user()->kinds->contains($request->id)) {
                return $next($request);
            } 
        } else {
            if (Auth::user()->kinds->contains($request->kind_id)) {
                return $next($request);
            }
        }
        return redirect()->back()->withErrors([
            'error' => 'Kind is not in your list!'
        ]);
    }
}
