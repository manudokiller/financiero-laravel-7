<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CurrencyByUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->currency) {
            if(!$request->currency->default_currency_id && $request->currency->user_id) {
                return $next($request);
            }
            return redirect(route('currency.index'))->withErrors([
                'error' => 'You are not allowed to update this currency!'
            ]);
        } elseif ($request->id) {
            if (Auth::user()->currencies->contains($request->id)) {
                return $next($request);
            } 
        } else {
            if (Auth::user()->currencies->contains($request->currency_id)) {
                return $next($request);
            }
        }
        return redirect()->back()->withErrors([
            'error' => 'Currency is not in your list!'
        ]);
    }
}
