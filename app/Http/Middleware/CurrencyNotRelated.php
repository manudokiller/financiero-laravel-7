<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CurrencyNotRelated
{
    /**
     * Verify that a Currency is related to the Auth User
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->currencies->contains($request->currency->id)) {
            return $next($request);
        }
        return redirect(route('currency.index'))->withErrors([
            'error' => 'You don\'t have access!'
        ]);
    }
}
