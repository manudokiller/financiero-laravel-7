<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class KindNotRelated
{
    /**
     * Verify that a Kind is related to the Auth User
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->kinds->contains($request->kind->id)) {
            return $next($request);
        }
        return redirect(route('kind.index'))->withErrors([
            'error' => 'You don\'t have access!'
        ]);
    }
}
